﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample_RPG_Framework.CharacterClasses
{
    public enum Gender { Male, Female, Other}

    public abstract class Entity
    {
        protected string name;

        protected Entity gender;

        protected int strength;

        protected int dexterity;

        protected int knowledge;

        protected int health;

        protected int healthmodifier;

        public string Name
        {
            get { return name; }
            protected set { name = value; }
        }

        public string Gender
        {
            get { return Gender; }
            protected set { Gender = value; }
        }
        
        public int Strength
        {
            get { return strength; }
            protected set { strength = value; }

        }

        public int Dexterity
        {
            get { return dexterity; }
            protected set { dexterity = value; }
        }

        public int Knowledge
        {
            get { return knowledge; }
            protected set { knowledge = value; }
        }

        public int Health
        {
            get { return health + healthmodifier; }
            protected set { health = value; }
        }


        public Entity()
        {
            Name = "";
            Strength = 0;
            Knowledge = 0;
            Dexterity = 0;
            Health = 0;
           
        }
            
       


    }
}
