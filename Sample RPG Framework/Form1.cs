﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Sample_RPG_Framework
{



    public partial class Form1 : Form
    {

        int gridX = 9;
        int gridY = 9;
        string filepath  = Application.StartupPath + "\\maptest.txt";
        PictureBox[,] mapGrid = new PictureBox[10,10];
        MapStuff[,] mapStuff = new MapStuff[10,10];
        int dudeX = 0;
        int dudeY = 0;
        Color dudeColor = Color.RoyalBlue;


        Random weapon = new Random();
        Random monster = new Random();


        public Form1()
        {
            
            InitializeComponent();
            this.KeyDown += Form1_KeyDown;
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            mapGrid[dudeX, dudeY].BackColor = getMapColor(mapStuff[dudeX, dudeY].getColor());
            
            switch (e.KeyCode)
            {
                case Keys.W:
                    
                    if (dudeY > 0)
                    {
                        if (mapStuff[dudeX, dudeY - 1].getCanPass())
                        {
                            dudeY--;
                        }
                    }
                    break;

                case Keys.S:

                    if (dudeY < gridY)
                    {
                        if (mapStuff[dudeX, dudeY + 1].getCanPass())
                        {
                            dudeY++;
                        }
                    }
                    break;

                case Keys.A:

                    if (dudeX > 0)
                    {
                        if (mapStuff[dudeX-1, dudeY].getCanPass())
                        {
                            dudeX--;
                        }
                    }
                    break;

                case Keys.D:

                    if (dudeX < gridX)
                    {
                        if (mapStuff[dudeX + 1, dudeY].getCanPass())
                        {
                            dudeX++;
                        }
                    }
                    break;

                default:
                    break;
            }

            mapGrid[dudeX, dudeY].BackColor = dudeColor;
           
        }

        public struct MapStuff
        {
            int x { get; set; }
            int y { get; set; }
            string terrain { get; set; }
            public bool canpass { get; set; }
            string item { get; set; }

            public void setX(string x)
            {
                this.x = int.Parse(x);
            }
            public void setY(string y)
            {
                this.y = int.Parse(y);
            }
            public string getColor()
            {
                return this.terrain;
            }
            public void setCanPass(string canPass)
            {
                this.canpass = bool.Parse(canPass);
            }
            public void setItem(string item)
            {
                this.item = item;
            }
            public void setTerrain(string terrain)
            {
                this.terrain = terrain;
            }
            public bool getCanPass()
            {
                return this.canpass;
            }
        }

        private void createMap()
        {
            for (int x = 0; x <= gridX; x++)
            {
                for (int y = 0; y <= gridY; y++)
                {
                    PictureBox pic = new PictureBox();
                    pic.Left = 30 * x + 10;
                    pic.Top = 30 * y + 10;
                    pic.Height = 29;
                    pic.Width = 29;
                    mapGrid[x, y] = pic;
                    pic.BackColor = Color.Plum;
                    this.Controls.Add(pic);
                }
            }
        }

        private void draw_map()
        {
            for(int x = 0; x<= gridX; x++)
            {
                for (int y = 0;y <= gridY; y++)
                {
                    mapGrid[x, y].BackColor = getMapColor(mapStuff[x, y].getColor());
                }
            }
        }

        private Color getMapColor(string mapTerrain)
        {
           
            label3.Text =  mapTerrain;

            if (mapTerrain == "lava")
            {
                return Color.Red;
                

            }
            else if(mapTerrain == "grass")
            {
                return Color.LawnGreen;
            }
            else if (mapTerrain == "dirt")
            {
                return Color.Brown;
            }
            else if (mapTerrain == "forest")
            {
                return Color.DarkGreen;
            }
            else if (mapTerrain == "mountain")
            {
                return Color.Gray;
            }
            else if (mapTerrain == "water")
            {
                return Color.Aquamarine;
            }
            else if (mapTerrain == "door")
            {
                return Color.Beige;
            }
            else if (mapTerrain == "monster")
            {
                return Color.LawnGreen;
                
            }
            else if (mapTerrain == "strength up")
            {
                return Color.AliceBlue;
            }
      
            else
            {
                return Color.Black;
            }

        }

        private void loadMap()
        {
            StreamReader obj = new StreamReader(filepath);
            string lineIn = "";
            string[] tmp;

            while(obj.Peek() != -1)
            {
                lineIn = obj.ReadLine();
                tmp = lineIn.Split('|');
                mapStuff[int.Parse(tmp[0]), int.Parse(tmp[1])].setX(tmp[0]);
                mapStuff[int.Parse(tmp[0]), int.Parse(tmp[1])].setY(tmp[1]);
                mapStuff[int.Parse(tmp[0]), int.Parse(tmp[1])].setTerrain(tmp[2]);
                mapStuff[int.Parse(tmp[0]), int.Parse(tmp[1])].setCanPass(tmp[3]);
                mapStuff[int.Parse(tmp[0]), int.Parse(tmp[1])].setItem(tmp[4]);
            }

            obj.Close();

        }

    

        private void Form1_Load(object sender, EventArgs e)
        {
            createMap();
            loadMap();
            draw_map();

            mapGrid[dudeX, dudeY].BackColor = dudeColor;
        }


        private void strength(string mapTerrain)
        {
            if (mapTerrain == "strength up")
            {
                MessageBox.Show("Strength UP!!");
                lblStrengthPnt.Text = (double.Parse(lblStrengthPnt.Text) == 0).ToString();
            }
        }

        private void combat(string mapTerrain)
        {
            if (mapTerrain == "monster")
            {

                  MessageBox.Show("A monster has set foot upon you");
                  lblHealthPnt.Text = (double.Parse(lblHealthPnt.Text) - 10).ToString();


                if (lblStrengthPnt.Text == "40")
                {
                    MessageBox.Show("The monster beat you unluggy - 30 health");
                    lblHealthPnt.Text = (double.Parse(lblHealthPnt.Text) == 0).ToString();

                }
                if (lblHealthPnt.Text == "0")
                {
                    MessageBox.Show("You dead son, unluggy");
                }
              
            }


        }



        private void button1_Click(object sender, EventArgs e)
        {
            button1.Visible = false;
            comboBox1.Visible = false;
            label1.Visible = false;
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
